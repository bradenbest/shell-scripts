# convert/ytdl -- Download and convert YouTube videos to mp3s quickly

These are meant for downloading from (and converting from .m4a/other to .mp3) YouTube. They are part of the [bash modules thing](https://gitlab.com/bradenbest/import) I made a long time ago. I dunno why I thought it would be a good idea to create dependencies. The main appeal of bash-import is that you can put scripts in a single directory, and dependencies (like avconv, youtube-dl, vlc, etc.) are automatically checked for. But since you can just put them in /usr/local/bin/ and use `. module` anyway, it's not very useful.

Includes:

* convert
* ytdl
* sound - the module that ytdl and convert use

You can still use the sound module with simply `. sound` or `source sound`, as you can all the bash modules I made for bash-import

## Installation

Deploy where you intend to use (for instance, ~/Music/.tmp/), and invoke with `./convert` or `./ytdl [url]`
