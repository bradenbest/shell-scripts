## dupefinder -- Find duplicate files in a directory

This is a bash script that finds duplicate files (pictures, music, anything), within a directory.

## Install

    sudo ./install

## Usage

Let's say you have a directory called "music", and you download a lot of music from people. However, due to the way the website is set up, there is a "reshare" feature which ends in you downloading duplicate files with very similar filenames. Some of them even have completely different filenames.

*Why would I need this?*

Okay, fine, you have 24,000 different files. Good luck sorting through that manually.

    dupefinder music/ #<-- use tab completion or at least make sure to include the / at the end of the directory

The script has two modes. Quick scan and Deep scan. It will always perform a quick scan first, and if it finds duplicates, will jump into deepscan. Though if it comes out clean, there is still the option to override.

If you DO have a lot of files (>500), and it goes into deep scan, go ahead and get something to drink/snack, and do something else, cause it takes a while, checking every file against every file, and all.
