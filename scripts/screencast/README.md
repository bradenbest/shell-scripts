# screencast -- a simple screencasting tool

Uses mplayer, ImageMagick's convert utility, and recordmydesktop to make a gif screencast of any window.

![Example](example.gif)

## Installation

    $ sudo ./install

## Usage

    $ screencast

Then just follow the instructions. Simple.

## Troubleshooting

Sometimes, recordmydesktop will fail, with the error "Segmentation Fault." When this happens, your desktop background will be stuck as a solid black. To remedy this, look at the last line of `screencast.log` (it will be in the directory where you executed screencast), and run `kill -9 [id]` using the id you got. Now you can change your desktop background back.

Make sure to stop recordmydesktop with Ctrl+Alt+s, and not Ctrl+c. The latter will terminate the entire script, and once again, you'll have to do the above to terminate the loop that's keeping your desktop black.
