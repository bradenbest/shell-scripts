# mp3tag -- tag a directory of mp3s quickly

This is for tagging a directory full of mp3s for easy use with cmus, which, as anyone whom has used it knows, can be finicky with less-than-exact tagging.

As per the example included in the file, it takes a directory as an argument

    $ mp3tag zircon/return-all-robots/
