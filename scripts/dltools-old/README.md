# dltools - the old version using a ton of shell scripts

Actually, these are still useful on their own. You could even combine some of them with the [new tools](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/dltools)

## check

check files for existence. To make sure you didn't skip a number accidentally (rendered redundant with dloop's auto-numbering)

## dl

for use with openabunchofxterms. Runs wget in a simplified way. invoke with ./dl '[url]' [episode-number].mp4

## list [Useful on its own]

It's a good way to get a neat look at all the shows and movies you have downloaded. It assumes that you put each show into their own directory, of course.

## mkdir 

makes a directory with symbolic links to dl (as dl) and openabunchofxterms (as xt), as well as n auto-generated script called "rm", which removes the symbolic links and itself for cleanup

## openabunchofxterms 

the silliest, and yet most specific, script name in the world. Does exactly what the name says. It asks you for a number, and then opens xterms in the background using the command you provide (which is going to be something like ./dl 'url' XX.mp4)

There's a reason I trashed these for the new tools. dloop combines openabunchofxterms and dl with much more robust functionality. It also doesn't waste resources by opening a bunch of GUI windows, and instead sends them in the background.

## watch 

Grabs the first video from each directory in the list given, and plays (then removes) them sequentially.

This was superceded by a [different project](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/qwatch), which takes that functionality up to 11.