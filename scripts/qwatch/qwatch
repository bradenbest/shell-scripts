#!/bin/bash
# vim: foldmethod=marker

# qwatch (quick watch) - watch videos quickly and comfortably with this powerful, feature-packed shell script
# Copyright (c) 2015 Braden Best

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# This software is released under the GPL license.
# For more details, visit http://gnu.org/

# GLOBAL VARIABLES {{{

# temporary variables
__pwd=$(pwd)
__dir=${1:-$__pwd}

# == USER CONFIG ==
# You can copy these lines to your qwatch.conf and they will override anything this script comes up with

# General stuff
limit=0
filter=
# if set to 1, autoplay will run automatically
autoplay=0
# the first command to run on a selected file, aka the 'viewer command'
vwr="vlc --fullscreen --play-and-exit"
# the second command to run on a selected file, aka the 'after command'
_rm="rm -i"
# the command that autoplay sets _rm to
_rm_autoplay="rm"
# Delay to wait for user to press Ctrl+C while in autoplay
autoplay_delay=1
# If set to 0, color will be disabled
use_color=1
# if set to 0, the 'ls' command will run automatically before every prompt
no_list=1
# Recommended so you can stay on the cutting edge
auto_update=1
# For use with the #[command] command. Set to 1 to allow access to all variables. Default is 0
preserve_environment=0
# Change this if you don't like the default pagers/have your own preference. Pager must work in the form "output | pager", so vim won't work. Trust me, I tried. Even with 'vim -', and making a shell script that copies stdin to a file and opens it with vim.
# Note also that whatever you change your pager to won't affect commands like 'qwatch --help', since arguments are read -before- the config file.
pagers="less most more cat"

# Dmenu stuff
_dmenu=dmenu     # the executable to use for dmenu. For example, try slmenu: https://bitbucket.org/rafaelgg/slmenu/downloads
USE_DMENU=0      # Whether to use dmenu
DMENU_WARNINGS=1 # Warnings. Disable if they are annoying
DMENU_LINES=0    # Lines. Use if you want dmenu to list vertically rather than horizontally. Otherwise, 0 = horizontal
DMENU_HISTORY=0  # If set to 1, qwatch will aggregate everything you pass through dmenu into a file. This is a shotgun approach

# Prompt Line
# the following lines enable/disable parts of the prompt
prompt_version=1
prompt_path=1
prompt_limit=1
prompt_filter=1
prompt_vwr=1
prompt_rm=1
prompt_one_line=0
# the following lines change the colors of the parts of the prompt
# 30=black
# 31=red
# 32=green
# 33=yellow
# 34=blue
# 35=magenta
# 36=turquoise
# 37=white
prompt_version_color=31 # red
prompt_path_color=32    # green
prompt_limit_color=34   # blue
prompt_filter_color=34  # blue
prompt_vwr_color=33     # yellow
prompt_rm_color=31      # red
# this last line controls what the end of the prompt looks like.
prompt_head=">>"
# == END USER CONFIG ==

# These shouldn't be modified.
LINES=$(tput lines)
COLUMNS=$(tput cols)
CMD_LIST="r ? $ / @ ls fn ap cd yt sh # vwr spd lim rm d update source h help ! q"
DMH_FILE="$HOME/.qwatch/history"
CONFIG_FILE="$HOME/.qwatch/qwatch.conf"
update_path="https://gitlab.com/bradenbest/qwatch/raw/master"
VERSION=1.26.12
VERSION_STUB=12612
VERSION_MSG="
qwatch v1.25 - improved notifications, bugfixes
qwatch v1.26 - overhauled arguments, updater, and help

Arguments: major improvements
  Now you can put arguments in any order

--help can now be used with an argument in the same way as the 'help'
command.

    \`qwatch --help \"Auto Update\"\`

is equivalent to:

    \`help:Auto Update\`

But \`qwatch --help\` by itself retains its original function

--version now prints the version number, version stub, and version
message (changelog) all-in-one.

New arguments:
  --directory/-d
  --no-auto-update/-nau

Dependency check expanded.
+    git
+    less
+    notify-send
+    python 
+    vlc
+    wget
+    youtube-dl

I plan to deprecate DMENU support in favor of autocompletion in python

Special case for vlc silently sets 'vwr' to 'no_vlc'

new function no_vlc() prints an error message informing the user of the
lack of VLC.

completely rewrote update()
    Can't test it until I push the next commit. This might be problematic
added printf() to use printf with multiple lines

Updated docs to reflect the new arguments and deprecation of dmenu

changed qwatch.info to doc/ directory. Installer and updater
now build qwatch.info from doc/ before installing

v1.26.[1-7]
Debugging implementation of new updater

v1.26.8
New bash completion for command (internal completion yet to come)
updated installer to integrate bash completion
updated internal updater (update()) to install bash completion

v1.26.9
Tiny bugfix - wget's output was showing when downloading completion/qwatch

v1.26.10
Added --update

v1.26.11
updater now cleans the /tmp directory of qwatch* before starting up

v1.26.12
preparations for move
"

# END VARIABLES }}}
# Notes {{{
# adding a new command has become this workflow:
  #   fun->[new function]  -- the new function
  #   fun->qw_main->prompt -- for qw_main to accept the command
  #   var->CMD_LIST        -- for autocomplete/dmenu
  #   fun->qw_help->docs   -- for it to appear in help
  #   qwatch.info          -- detailed documentation
# commands using get_input(): qw_main, qw_help, qw_d, qw_cd, qw_yt, qw_vwr, qw_spd, qw_rm
# }}}
# FUNCTIONS {{{
# The QWatch Library {{{
qw_ap(){ # ------------ autoplay {{{
    autoplay=1
    _rm_before_autoplay=$_rm
    _rm="$_rm_autoplay"
    return 0
} #}}}
qw_cd(){ # ------------ change directory {{{
    if [[ $1 == -q ]]; then
        $choice # interpret literally
        return 0
    fi
    qwcdlist(){
        echo "current: $(pwd)"
        echo "available: "
        qw_list du | cat -n
    }
    if [[ $(qwcdlist | wc -l) -ge $LINES ]]; then
        qwcdlist | $(pager)
    else
        qwcdlist
    fi
    printf "Enter the number of the directory to go to "
    gi_prompt="Enter the number of the directory to go to"
    get_input $(seq 1 $(qw_list di | wc -l))
    if ! string_is_empty $choice; then
        cd "$(qw_list di | head -$choice | tail -1)"
    fi
    return 0
} #}}}
qw_cmds(){ # ---------- auto-suggestion generator for qw_main() for dmenu {{{
    if ! var USE_DMENU; then return; fi
    local _h
    echo $CMD_LIST                   # standard commands
    seq 1 $(qw_list | wc -l)         # video ids
    for _h in $filter_history; do    # past filters
        echo "/$_h"
    done
    for _h in $limit_history; do     # past limits
        echo "@$_h"
    done
    return 0
} # }}}
qw_d(){ # ------------- delete {{{
    if [[ $1 == -q ]]; then
        vid="$( qw_list | head -${choice:2} | tail -1 )"
        rm -i "$vid" 2>&1
        return 0
    fi
    printf "Enter id to delete "
    gi_prompt="Enter id to delete"
    get_input $(seq 1 $(qw_list | wc -l))

    vid="$( qw_list | head -$choice | tail -1 )"
    rm -i "$vid" 2>&1
    return 0
} #}}}
qw_default(){ # ------- for catching numbers and blanks {{{
    if choice_in '$'; then
        vid="$( qw_list | tail -1 )"
        return 0
    fi
    choice=${choice:-1}
    vid="$( qw_list | head -$choice | tail -1 )"
    return 0
} #}}}
qw_filt(){ # ---------- filter listing {{{
    if [[ $1 == -q ]]; then # q for quick
        filter=${choice:1}
        filter_history="$filter_history $filter"
    else
        printf "enter filter "
        gi_prompt="enter filter"
        get_input "$filter_history"
        filter=$choice
        filter_history="$filter_history $filter"
    fi
    return 0
} # }}}
qw_fn(){ # ------------ display filename of given number {{{
    if [[ $1 == -q ]]; then
        choice=${choice:3}
    else
        printf "Enter the number of the video "
        gi_prompt="Enter the number of the video"
        get_input $(seq 1 $(qw_list | wc -l))
    fi
    echo "$(qw_list | cat -n | head -$choice | tail -1)"
    return 0
} # }}}
qw_help(){ # ---------- list commands {{{
    echo "For more detailed help, run 'qwatch --help'"
    echo
    echo "Use the numbers to the left of the files to select them, or..."
    echo
    echo "  command  args       description"
    echo "--------------------------------------------------------------------------------"
    echo "           <>         first item"
    echo "        $  <>         last item"
    echo "        ?  <>         random item"
    echo "        !  <>         repeat last command"
    echo "       ap  <>         autoplay (watch each video and delete without any prompts)"
    echo "       cd  <dir>      go to another directory"
    echo "        d  <id>       delete"
    echo "       fn  <id>       show filename of number of <id>"
    echo "     help  <topic>    show detailed help on <topic>"
    echo "        h  <>         show this help"
    echo "      lim  <>         change list length limit (@n also works)"
    echo "        /  <filter>   filter listing (no space between / and <filter>)"
    echo "       ls  <>         list files (same as --auto-list)"
    echo "        q  <>         quit"
    echo "       rm  <command>  change rm method"
    echo "        r  <>         refresh"
    echo "       sh  <>         jump into a shell"
    echo "        #  <command>  interpret command with shell"
    echo "   source  <file>     source a config file/script"
    echo "      spd  <number>   change playback speed (vlc only)"
    echo "   update  <>         run the updater"
    echo "      vwr  <command>  change viewer"
    echo "       yt  <>         watch from youtube"
    echo
    if [[ $commands ]]; then
        echo "User-defined:"
        for i in ${commands[@]}; do
            cmd=$(command_get_val $i)
            cmdh=$($cmd help)
            printf "    $(command_get_key $i)\t ${cmdh}\n"
        done
        echo
    fi
    echo "For more information, see 'help:Commands' and 'help:Custom Commands'"
    return 0
} #}}}
qw_info(){ # ---------- detailed help, access with --help {{{
    if var USE_HOME; then
        local qw_dir=$HOME/.qwatch
        cp $qw_dir/qwatch.info /tmp/qw.tar
        (
            cd /tmp
            tar xf qw.tar
            cd qwatch.info/
            if [[ $1 == --topic ]]; then
                choice="help:$2" # wrap it so the topic does not have to be defined twice
            fi
            if [[ $1 == -q ]] || [[ $1 == --topic ]]; then
                local topic=$(echo ${choice:5} | tr ' ' '-' | tr '[:upper:]' '[:lower:]') # Auto Update -> auto-update
                if [[ -e $topic ]]; then
                    cat $topic
                else
                    echo "No help topic for '$topic'"
                    cat table-of-contents
                fi
            else # 'qwatch --help'
                local full_manual=(
                    table-of-contents
                    version-and-copyright
                    arguments
                    commands
                    custom-commands
                    dmenu-integration
                    the-prompt
                    notifications
                    custom-functions
                    user-configuration
                    auto-update
                    bugs
                    legal-information
                    maintainer-information
                    additional-commentary
                    full-license
                )
                local manual
                for manual in ${full_manual[@]}; do
                    cat $manual
                done
            fi
        ) | awk -v VERSION="$VERSION" -v LINES="$(cat $0 | wc -l)" -v LICENSE="$(cat $qw_dir/LICENSE)" '{
            sub(/\$VERSION/, VERSION);
            sub(/\$LINES/,   LINES);
            sub(/\$LICENSE/, LICENSE);
            print;
        }' | $(pager)
    else
        echo "Due to a broken installation, help is disabled."
        echo "Consider uninstalling and reading the README.md to get everything set up right."
    fi
    return 0
} # }}}
qw_lim(){ # ----------- change limit {{{
    if [[ $1 == -q ]]; then
        limit=${choice:1}
        limit_history="$limit_history $limit"
    else
        printf "Set new limit (0 = no limit, blank to cancel) "
        gi_prompt="set new limit, 0 for no limit, or press esc to cancel"
        get_input "0 $limit_history"
        limit_history="$limit_history $choice"
        if ! string_is_empty $choice; then
            limit=$choice
        fi
    fi
    return 0
} #}}}
qw_list(){ # ---------- list files or directories (not a prompt command) {{{
    if [[ $1 == du ]]; then # dirctory (user view)
        qw_list_fn(){ find -maxdepth 1 -type d | sort -V; }
        local whitespace_length=$(get_longest $(qw_list_fn))
        printf "%-${whitespace_length}s %s\n" ".." "[$(ls .. | wc -l)]"
        printf "%-${whitespace_length}s %s\n" "." "[$(ls | wc -l), $(find | wc -l)]"
        local tot=$(qw_list_fn | wc -l)
        local count=2
        local ln=
        while [[ $count -le $tot ]]; do
            ln="$(qw_list_fn | head -$count | tail -1)"
            printf "%-${whitespace_length}s %s\n" "$ln" "[$(ls $ln | wc -l), $[$(find $ln | wc -l) - 1]]"
            count=$[count + 1]
        done
        unset -f qw_list_fn
    elif [[ $1 == di ]]; then # directory (internal view)
        echo ".."
        find -maxdepth 1 -type d | sort -V
    else
        if var limit 0; then
            find -type f | sort -V | grep "$filter"
        else
            find -type f | sort -V | grep "$filter" | head -$limit
        fi
    fi
    return 0
} #}}}
qw_list_run(){ # ------ run qw_list (file mode) in pager or standalone {{{
    if [[ $(qw_list | wc -l) -ge $LINES ]]; then
        qw_list | cat -n | $(pager)
    else
        qw_list | cat -n
    fi
    return 0
} # }}}
qw_mkprompt(){ # ------ Make prompt based on configuration #{{{
    local __prompt=
    prompt(){
        [[ $1 == -a ]] && mode=and || mode=or
        shift
        for arg in $@; do
            str=prompt_$arg
            if [[ ${!str} == 1 ]]; then
                [[ $mode == or  ]] && return 0
            else
                [[ $mode == and ]] && return 1
            fi
        done
        [[ $mode == and ]] && return 0 || return 1
    }
    prompt -o version && __prompt="$__prompt[$(color $prompt_version_color "qwatch v$VERSION")] "
    prompt -o path    && __prompt="$__prompt$(color  $prompt_path_color    "$(pwd)") "
    prompt -o limit   && __prompt="$__prompt@$(color $prompt_limit_color   "$limit") "
    prompt -o filter  && __prompt="$__prompt/$(color $prompt_filter_color  "$filter") "
    prompt -o vwr rm  && __prompt="$__prompt["
    prompt -o vwr     && __prompt="$__prompt$(color $prompt_vwr_color "$vwr")"
    prompt -a vwr rm  && __prompt="$__prompt; "
    prompt -o rm      && __prompt="$__prompt$(color $prompt_rm_color  "$_rm")"
    prompt -o vwr rm  && __prompt="$__prompt]"
    printf "$__prompt"
    unset -f prompt
    return 0
} #}}}
qw_parse_command(){ # - Parse commands {{{
    cm(){ # Command Match
        local _start=${2:-0}
        local _end=${3:-$}
        [[ $_end == "$" ]] && _end=$(echo $choice | wc -c)
        [[ "${choice:$_start:$_end}" == "$1" ]]; return $?
    }
    if cm '!'; then # as '!' is a meta-command, it needs a special case
        echo "Using last command: $last_choice"
        choice="$last_choice"
    fi
    last_choice="$choice"
    while true; do # This is silly, but I'm using a loop so that I can break, since AFAIK there is no JMP or GOTO instruction in bash. It's an optimization.
        #  command  s e      loop control                 action
        #  -------  - -  --  -----------------------  --  ------
        cm ''       0 $  &&  qwpc_after="do_nothing"                    &&  break
        cm '$'      0 $  &&  qwpc_after="do_nothing"                    &&  break
        cm '?'      0 $  &&  qwpc_after="do_nothing"  &&  qw_rand       &&  break
        cm '!'      0 $  &&  qwpc_after="continue"                      &&  break
        cm 'ap'     0 $  &&  qwpc_after="continue"    &&  qw_ap         &&  break
        cm 'cd'     0 $  &&  qwpc_after="continue"    &&  qw_cd         &&  break
        cm 'cd'     0 2  &&  qwpc_after="continue"    &&  qw_cd -q      &&  break
        cm 'd'      0 $  &&  qwpc_after="continue"    &&  qw_d          &&  break
        cm 'd'      0 1  &&  qwpc_after="continue"    &&  qw_d -q       &&  break
        cm 'fn'     0 $  &&  qwpc_after="continue"    &&  qw_fn         &&  break
        cm 'fn'     0 2  &&  qwpc_after="continue"    &&  qw_fn -q      &&  break
        cm 'help'   0 4  &&  qwpc_after="continue"    &&  qw_info -q    &&  break
        cm 'h'      0 $  &&  qwpc_after="continue"    &&  qw_help       &&  break
        cm 'lim'    0 $  &&  qwpc_after="continue"    &&  qw_lim        &&  break
        cm '@'      0 $  &&  qwpc_after="continue"    &&  qw_lim        &&  break
        cm '@'      0 1  &&  qwpc_after="continue"    &&  qw_lim -q     &&  break
        cm '/'      0 $  &&  qwpc_after="continue"    &&  qw_filt       &&  break
        cm '/'      0 1  &&  qwpc_after="continue"    &&  qw_filt -q    &&  break
        cm 'ls'     0 $  &&  qwpc_after="continue"    &&  qw_list_run   &&  break
        cm 'q'      0 $  &&  qwpc_after="break"                         &&  break
        cm 'rm'     0 $  &&  qwpc_after="continue"    &&  qw_rm         &&  break
        cm 'rm'     0 2  &&  qwpc_after="continue"    &&  qw_rm -q      &&  break
        cm 'r'      0 $  &&  qwpc_after="continue"                      &&  break
        cm 'sh'     0 $  &&  qwpc_after="continue"    &&  qw_sh         &&  break
        cm '#'      0 $  &&  qwpc_after="continue"    &&  qw_sh         &&  break
        cm '#'      0 1  &&  qwpc_after="continue"    &&  qw_sh -q      &&  break
        cm 'source' 0 $  &&  qwpc_after="continue"    &&  qw_source     &&  break
        cm 'source' 0 6  &&  qwpc_after="continue"    &&  qw_source -q  &&  break
        cm 'spd'    0 $  &&  qwpc_after="continue"    &&  qw_spd        &&  break
        cm 'spd'    0 3  &&  qwpc_after="continue"    &&  qw_spd -q     &&  break
        cm 'update' 0 $  &&  qwpc_after="continue"    &&  update        &&  break
        cm 'vwr'    0 $  &&  qwpc_after="continue"    &&  qw_vwr        &&  break
        cm 'vwr'    0 3  &&  qwpc_after="continue"    &&  qw_vwr -q     &&  break
        cm 'yt'     0 $  &&  qwpc_after="continue"    &&  qw_yt         &&  break
                                                                            break
    done
    # User-defined commands
    for i in ${commands[@]}; do
        if cm $(command_get_key $i) 0 $; then
            cmd=$(command_get_val $i)
            cmda=$($cmd after)
            qwpc_after=$cmda
            string_is_empty $cmda && \
                qwpc_after="continue"
            $cmd
            break
        fi
    done
    unset -f cm
    return 0
} #}}}
qw_rand(){ # ---------- set random video (no continue) {{{
    vid="$( qw_list | sort -R | head -1 )"
    qw_default_skip=1
    return 0
} #}}}
qw_rm(){ # ------------ change end-of-watch action (removal method) {{{
    if [[ $1 == -q ]]; then
        _rm=${choice:3} # rm [input]
        return 0
    fi
    echo "After watching videos, I will do this:"
    echo "0. remove"
    echo "1. prompt for removal (default)"
    echo "2. nothing"
    echo "3. something else"
    gi_prompt="set new do-after-play command"
    get_input "remove prompt-remove nothing custom"
    if choice_in "0" "r" "rm" "remove"; then
        _rm="rm"
    elif choice_in "2" "n" "do_nothing" "nothing"; then
        _rm="do_nothing"
    elif choice_in "3" "custom" "other"; then
        printf "Enter a command, function, or script "
        gi_prompt="Enter a command, function or script"
        get_input $(list_progs)
        _rm=$choice
    else
        _rm="rm -i"
    fi
    return 0
} #}}}
qw_sh(){ # ------------ shell {{{
    if [[ $1 == -q ]]; then
        if ! var preserve_environment; then
            echo "${choice:1}" | ${SHELL:-/bin/sh}
        else
            eval "${choice:1}"
        fi
        return 0
    fi
    export limit filter
    export -f qw_{list,default}
    echo "additional functions available: qw_list, qw_default"
    echo "usage:"
    echo "    qw_list | cat -n"
    echo "    choice=[num]"
    echo "    qw_default"
    echo "    echo \$vid"
    ${SHELL:-/bin/sh} 2>&1
    return 0
} #}}}
qw_source(){ # -------- source a config file {{{
    if [[ $1 == -q ]]; then
        choice=${choice:7}
    else
        printf "Enter file to source "
        gi_prompt="Enter file to source"
        get_input $(ls *.conf)
    fi
    source "$(expand "$choice" '~' "$HOME")" 2>&1
    return 0
} #}}}
qw_spd(){ # ----------- change speed {{{
    if [[ $1 == -q ]]; then # spd [input]
        vwr="vlc --play-and-exit --fullscreen --rate ${choice:4}"
        return 0
    fi
    printf "enter new speed "
    gi_prompt="enter new speed"
    get_input 0.5 1.0 1.5 2.0 3.0 4.0
    if string_is_empty $choice; then return; fi
    vwr="vlc --play-and-exit --fullscreen --rate $choice"
    return 0
} #}}}
qw_vwr(){ # ----------- change viewer {{{
    if [[ $1 == -q ]]; then
        vwr="${choice:4}" # vwr [input]
        return 0
    fi
    echo "current: $vwr"
    choice=""
    printf "enter new viewer "
    gi_prompt="enter new viewer command"
    get_input $(list_progs) '!'
    choice=$(expand "$choice" '!' "$vwr")
    while ! app_exists $choice && ! string_is_empty $choice; do
        echo "$choice not found"
        printf "try again "
        gi_prompt="[!] enter new viewer command"
        get_input $(list_progs) '!'
        choice=$(expand "$choice" '!' "$vwr")
    done
    if string_is_empty $choice; then return; fi
    vwr="$choice"
    if choice_in "vlc"; then
        printf "use '--play-and-exit --fullscreen'? [Y/n] "
        gi_prompt="use '--play-and-exit --fullscreen'?"
        get_input yes no
        if choice_in "n" "no"; then
            vwr="vlc --play-and-exit --fullscreen"
        fi
    elif choice_in "mplayer"; then
        printf "use '-vo fbdev2' for TTY output? [y/N] "
        gi_prompt="use '-vo fbdev2' for TTY output?"
        get_input yes no
        if choice_in "y" "yes"; then
            vwr="mplayer -vo fbdev2"
        fi
    fi
    return 0
} #}}}
qw_yt(){ # ------------ youtube-dl {{{
    if ! app_exists youtube-dl; then
        echo "This feature is disabled. To enable it, install youtube-dl"
        return 0
    fi
    if app_exists wget; then
        printf "Use wget? [y/n] "
        gi_prompt="Use wget?"
        get_input yes no
        if choice_in "y" "yes"; then
            qw_yt_wget
            return 0
        fi
    fi
    local args
    printf "video (v) or sound (s)? "
    gi_prompt="download the whole video or just the audio?"
    get_input video audio
    if choice_in "a" "s" "audio" "sound"; then
        args="--extract-audio"
    else
        args="-f 18"
    fi
    printf "[youtube-dl] enter one or more sources "
    gi_prompt="enter one or more sources"
    get_input $ytdl_history
    ytdl_history="$ytdl_history $choice"
    (
        cd /tmp/
        mkdir -p qwyt
        cd qwyt/
        youtube-dl $args --youtube-skip-dash-manifest $choice
        $vwr *
        rm *
    )
    return 0
} #}}}
qw_yt_wget(){ # ------- youtube-dl + wget {{{
    printf "[wget] enter one or more sources "
    gi_prompt="enter one or more sources"
    get_input $ytdl_history
    ytdl_history="$ytdl_history $choice"
    mkdir -p qwyt
    for i in $choice; do
        echo "source: $i"
        title="$(youtube-dl --get-title "$i" | sed "s/ /-/g")"
        fname=${title:0:15}.1.mp4
        url="$(youtube-dl --get-url "$i")"
        dupe=2
        while [[ -e qwyt ]] && [[ -e qwyt/$fname ]]; do
            fname="${title:0:15}.$dupe.mp4"
            dupe=$[dupe + 1]
        done
        echo "================================================================="
        echo "== saving '${url:0:20}...' to '$fname' =="
        echo "================================================================="
        ( cd qwyt && wget "$url" -O $fname )
    done
    return 0
} #}}}
qw_main(){ # ---------- Quick Watch - the main loop {{{
    while true; do
        qwpc_after="do_nothing"
        ! var no_list && qw_list_run # auto list
        if ! var limit 0; then # limit is set
            if choice_in "ls" || ! var no_list; then # qw_list was executed
                echo "    [ ... ]"
            fi
        fi
        if ! var autoplay; then # autoplay off
            echo "type 'h' for more options"
            printf "$(qw_mkprompt) "
            if [[ $prompt_one_line == 0 ]]; then
                echo
            fi
            gi_no_sort=1
            gi_prompt="Enter your command"
            get_input $(qw_cmds)
            qw_parse_command
            $qwpc_after
        else # autoplay on
            echo "Autoplay is on. Press Ctrl+C to stop or wait until all videos are finished"
            if [ $(qw_list | wc -l) -gt 0 ]; then
                choice=1
                qw_default
                echo "If you want to quit, press Ctrl+C now"
                sleep $autoplay_delay
            else # no more items in listing
                echo "Playback finished."
                autoplay=0
                filter=/
                limit=0
                _rm=$_rm_before_autoplay
                continue
            fi
        fi

        if var qw_default_skip; then
            # commands that set qwpc_after="do_nothing" must also set this flag to prevent qw_default from
            # overriding their settings
            echo "skipped default action"
            qw_default_skip=0
        else
            qw_default
            # apparently the case statement in qw_parse_command wasn't running
            # qw_default...for no reason whatsoever, so I'll just leave it here instead
        fi
        if ! string_is_empty $vid; then
            if notify --check && x_is_running; then
                notify "[Qwatch] Now Playing '$vid'"
            fi
            $vwr "$vid"  2>&1
            $_rm "$vid"  2>&1
        fi
    done
    return 0
} #}}}
# }}}
# Other Functions {{{
app_exists(){ # ------- Determine if app exists {{{
    which "$1" >/dev/null 2>&1
    return $?
} #}}}
choice_in(){ # -------- Check $choice against a list of strings {{{
    for item in $@; do
        if [[ $choice == $item ]]; then
            return 0
        fi
    done
    return 1
} #}}}
color(){ # ------------ Color some text. e.g. $(color 31 "hello") will return "hello" in red {{{
    if var use_color; then
        printf "\x1b[0;0;${1}m${2}\x1b[0;47;0m" # 0x1b (27) is the escape character
    else
        echo "$2"
    fi
    return 0
} #}}}
command_add(){ # ------ map a custom command to a function {{{
    # usage: command_add [command] [function]
    local length=$(echo ${commands[@]} | wc -w)
    commands[$length]="$1:$2"
    return 0
} # }}}
command_get_key(){ # -- get key in 'key:value' string {{{
    echo ${1%:*}
    return 0
} #}}}
command_get_val(){ # -- get value in 'key:value' string {{{
    echo ${1#*:}
    return 0
} #}}}
do_nothing(){ # ------- Does nothing {{{
    return 0
} # }}}
expand(){ # ----------- Expand special characters at the beginning of strings {{{
    # args: (1) input (2) magic symbol (3) what the magic symbol will expand to
    if [[ $2 == '!' ]]; then # ! = use last. Reasoning: Bash uses !! to repeat the last command. E.g. 'sudo !!' would expand to 'sudo [last command]'
        if [[ ${1:0:1} == '!' ]]; then
            echo "$3${1:1}"
        else
            echo "$1"
        fi
    elif [[ $2 == '~' ]]; then # ~ = user's home directory
        if [[ ${1:0:1} == '~' ]]; then
            echo "$3${1:1}"
        else
            echo "$1"
        fi
    fi
    return 0
} #}}}
get_input(){ # -------- get input using read or dmenu {{{
    if x_is_running && app_exists $_dmenu && var USE_DMENU; then
        # x_is_running() runs xhost and returns its return value
        # app_exists $_dmenu checks that dmenu exists
        # USE_DMENU is user-configured, but defaults to 0
        echo
        if var DMENU_HISTORY && [[ -e $DMH_FILE ]]; then
            local __dmenu_history=$(cat $DMH_FILE)
            __dmenu_history=$(
                ( for i in $__dmenu_history; do echo $i; done ) | sort -uV
            )
        fi
        if ! string_is_empty $gi_no_sort; then
            choice=$(
                ( for i in $@ $__dmenu_history; do echo $i; done ) | $_dmenu -l $DMENU_LINES -p "[qwatch v$VERSION] $gi_prompt"
            )
            gi_no_sort= # unset it
        else
            choice=$(
                ( for i in $@ $__dmenu_history; do echo $i; done ) | sort -uV | $_dmenu -l $DMENU_LINES -p "[qwatch v$VERSION] $gi_prompt"
            )
            # -u for unique, -V for version (numbers first, sorted 1,2,3...9,10,11)
        fi
        gi_prompt=
        if var DMENU_HISTORY; then
            echo "$__dmenu_history $choice" >> $DMH_FILE
        fi
    else
        if var USE_DMENU && var DMENU_WARNINGS; then
            if ! app_exists $_dmenu; then echo "the program '$_dmenu' is not on your system. You have to install it."; fi
            if ! x_is_running; then echo "you are in a TTY or otherwise not able to access X from here. Are you sharing a multiplexer with a TTY?"; fi
        fi
        read -ep "$prompt_head " choice 2>&1
    fi
    return 0
} # }}}
get_longest(){ # ------ get the length of the longest item in a list {{{
    local longest=0
    local len=
    for i in $@; do
        len=$(echo $i | wc -c)
        if [[ $len -gt $longest ]]; then
            longest=$len
        fi
    done
    echo $longest
    return 0
} # }}}
list_progs(){ # ------- list all programs in $PATH {{{
    if ! var USE_DMENU; then return; fi
    local list=
    IFS=':'
    for i in $PATH; do
        list="$list $(ls $i)"
    done
    IFS=$' \n\t' # default
    echo $list
    return 0
} # }}}
notify(){ # ----------- Sends notifications {{{
    if [[ $1 == --check ]]; then
        app_exists notify-send
        return $?
    fi
    notify-send -- "$1"
    return 0
} #}}}
no_vlc(){ # ----------- Error message for when VLC is missing #{{{
    echo "\
======================================================
Unable to play file '$1'. VLC is not installed
You have two options:
    1. install VLC
    2. set 'vwr' to something else in your qwatch.conf 
       (see 'help:User Configuration')
======================================================"
} #}}}
pager(){ # ------------ Selects pager {{{
    for p in $pagers ; do
        if app_exists $p; then
            echo $p
            return 0
        fi
    done
    return 0
} #}}}
string_is_empty(){ # -- Determine if string is empty {{{
    [[ $1 == "" ]]
    return $?
} #}}}
update(){ # ----------- Update #{{{
    rm -rf /tmp/qwatch*
    echo "[Update] See 'help:Auto Update' for more information"
    if ! app_exists wget || ! app_exists git; then
        echo "[Update] Disabled. To enable, install wget and git."
        return 0
    fi
    echo "[Update] Checking for updates..."
    local new_doc=/tmp/qwatch-update-doc
    local new_prg=/tmp/qwatch-update
    local old_doc=/tmp/qwatch.info/meta
    local new_comp=/tmp/qwatch-update-comp
    local old_comp=/usr/share/bash-completion/completions/qwatch
    # fetch
    wget $update_path/doc/meta -O $new_doc >/dev/null 2>&1
    wget $update_path/qwatch -O $new_prg >/dev/null 2>&1
    wget $update_path/completion/qwatch -O $new_comp >/dev/null 2>&1
    cp $HOME/.qwatch/qwatch.info /tmp/qw; (cd /tmp; tar xf qw; rm qw)
    # get info
    local current_version_doc=$(cat /tmp/qwatch.info/meta)
    local current_version_prg=$VERSION_STUB
    local new_version_doc=$([[ -e $old_doc ]] && cat $new_doc || echo 0)
    local new_version_prg=$(bash $new_prg --version-stub)
    # compare
    if  [[ $current_version_doc -lt $new_version_doc ]]; then
        local update_do_doc=1
    fi
    if [[ $current_version_prg -lt $new_version_prg ]]; then
        local update_do_prg=1
    fi
    local updated=0
    # update documentation
    if [[ $update_do_doc ]]; then
        echo "There is a new version of the documentation"
        echo "Fetching..."
        repo=/tmp/qwatch-update-new-repo
        git clone https://gitlab.com/bradenbest/qwatch.git $repo
        echo "Building..."
        local cur_dir=$(pwd)
        cd $repo
        cp -r doc qwatch.info
        tar zcvf qwatch.info.tgz qwatch.info
        echo "Installing..."
        sudo mv qwatch.info.tgz $HOME/.qwatch/qwatch.info -i 2>&1
        cd $cur_dir
        updated=1
    fi
    # update qwatch
    if [[ $update_do_prg ]]; then
        echo "There is a new version of qwatch"
        echo "Your version: $VERSION"
        echo "New version:  $(bash $new_prg --version-number)"
        echo "== What's new =="
        printf "$(bash $new_prg --version-msg)\n"
        echo "================"
        chmod +x $new_prg
        sudo mv $new_prg /usr/local/bin/qwatch -i 2>&1
        updated=1
    fi
    # update completer
    if [[ $(cat $new_comp | md5sum) != $(cat $old_comp | md5sum) ]]; then
        echo "There is a new version of the completer"
        sudo mv $new_comp $old_comp -i 2>&1
        updated=1
    fi
    # If no updates were needed, say "up to date"
    if ! var updated; then
        echo "Up to date."
        return 0
    fi
} #}}}
var(){ # -------------- check value of var {{{
    if string_is_empty $2; then
        [[ ${!1} == 1 ]]
        return $?
    else
        [[ ${!1} == $2 ]]
        return $?
    fi
} #}}}
x_is_running(){ # ----- Determine if x is running {{{
    #xhost >/dev/null 2>&1
    [[ $DISPLAY ]]
    return $?
} #}}}
# }}}
# END FUNCTIONS }}}
# SETUP {{{
# Sanity check: does ~/.qwatch/ exist? #{{{
if [[ -d $HOME/.qwatch ]]; then
    USE_HOME=1
else
    USE_HOME=0
fi
#}}}
# Arguments #{{{
opt(){
    for a in $@; do
        [[ $arg == $a ]] && \
            return 0
    done
    return 1
}

while [[ $1 ]]; do
    arg=$1
    if   opt --auto-list -al; then
        no_list=0
    elif opt --config -c; then
        CONFIG_FILE="$2"
        shift
    elif opt --directory -d; then
        __dir="$2"
        shift
    elif opt --help -h; then
        if [[ ${2:0:1} == '-' ]] || string_is_empty $2; then # is another option / empty, so use default
            qw_info 
            exit
        else
            qw_info --topic "$2"
            exit
        fi
    elif opt --no-auto-update -nau; then
        auto_update=0
    elif opt --no-config -nc; then
        no_config=1
    elif opt --update -up; then
        update
        exit
    elif opt --version -v; then
        echo "qwatch v$VERSION (r$VERSION_STUB) (c) 2015 Braden Best"
        echo "Changelog:"
        printf "$VERSION_MSG"
        exit
    elif opt --version-number -vn; then
        echo "$VERSION"
        exit
    elif opt --version-msg -vm; then
        printf "$VERSION_MSG"
        exit
    elif opt --version-stub -vs; then
        echo "$VERSION_STUB"
        exit
    else
        __dir="$1"
    fi
    shift
done

unset -f opt
#}}}
# Check dependencies #{{{
if ! app_exists vlc; then
    vwr=no_vlc
    _rm=do_nothing
fi

depends=(
    git
    less
    notify-send
    python 
    vlc
    wget
    youtube-dl
)
depends_info=(
    "[git]           Updater unavailable."
    "[less]          Pager will default to more or cat"
    "[notify-send]   Desktop notifications unavailable (see 'help:Notifications' for more info)"
    "[python]        Python command completion unavailable"
    "[vlc]           Unable to watch videos (!)"
    "[wget]          Updater unavailable; 'yt' command will not use wget mode"
    "[youtube-dl]    'yt' command is unavailable"
)
missing_p=0
dep_p=0

for dep in ${depends[@]}; do
    if ! app_exists $dep; then
        missing[$missing_p]=$dep_p
        missing_p=$((missing_p + 1))
    fi
    dep_p=$((dep_p + 1))
done

if [[ $missing_p -gt 0 ]]; then
    echo "There are missing dependencies. Here's a list of effects:"
    for i in ${missing[@]}; do
        printf "    ${depends_info[$i]}\n"
    done
    echo
fi
#}}}
# Attempt to change directory #{{{
[[ -d $__dir ]] && cd -- $__dir
#}}}
# unset temps #{{{
unset __pwd __dir
#}}}
# User config override #{{{
if ! var no_config; then
    [[ -e $CONFIG_FILE ]] && source $CONFIG_FILE
    if [[ $(ls $HOME/.qwatch/functions/ | wc -l) -gt 0 ]]; then
        for script in $HOME/.qwatch/functions/*; do
            source $script
        done
    fi
else
    echo "user-defined functions and config disabled. "
fi
#}}}
# Live-mode (what config is meant to work on) starts here.
# Auto update #{{{
if ! var auto_update; then
    echo "[Auto Update] Disabled. To re-enable, set 'auto_update' to 1"
else
    update
fi
#}}}
# END SETUP }}}

qw_main 2> /dev/null # To prevent errors from polluting the display, e.g. dvtm seems to love broken pipes.
