## qwatch -- Quick Watch

### Installation

If this is a fresh install, run this first to set up:

    $ ./install -s

After or otherwise,

    $ sudo ./install    # to install or update
    $ sudo ./install -u # to uninstall

To make yourself a good config file, open qwatch in a text editor to around `line 30` or so, and copy the "USER CONFIG" section into ~/.qwatch/qwatch.conf. 

You can also take a look at the example_scripts/ directory for some snippets you can add to ~/.qwatch/functions/

### Updating

As mentioned above, you can clone/pull this repo to get the latest changes and run...

    $ sudo ./install

...to update.

As of v1.17, you can just run `qwatch`, and it will check for updates automatically. If it finds an update, it will download and install it. This is true for both the script itself and the documentation.

For more information, see `help:Auto Update`

In layman's terms, that means run `qwatch --help` and read the section titled `Auto Update`

### Usage

    $ qwatch [directory]

The directory is optional, qwatch is itself a command line.

For a list of commands, type 'h'.

For the full manual, run

    $ qwatch --help

This can be used with multiple kinds of media, as the viewer itself can be changed to anything. For example...

    vlc (default) - for videos, music
    aplay - for listening to raw audio
    mplayer - for music and videos
    firefox - to view an swf
    animate - to view a gif (requires imagemagick)
    feh - pictures
    fbi - pictures (when in TTY)
    cat - text files
    vi - editing text files

Default viewer is `vlc --fullscreen --play-and-exit`
