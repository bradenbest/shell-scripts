# dltools - some functions to aid in downloading large amounts of videos

Usage is quite simple. I packed it full of help info. To use, invoke with

    . dltools
    # or
    source dltools

This will insert its functions into your current shell session, and automatically run the help

    [dltools] Functions:
      [ ... ]
 
Just as it says, run `help [command]` to see more detailed info on a given command.

The help is also formatted at the standard 72-character line wrap used by GNU software, so it's nice and readable if you decide to read it in a 1970's UNIX terminal (or a floating xterm that hasn't been resized).
