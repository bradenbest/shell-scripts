## Braden's Shell Scripts

base url: https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/

* [mp3tag](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/mp3tag) -- tag a directory of mp3s quickly
* [convert and ytdl](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/convert-ytdl) -- Download and convert YouTube videos to mp3s quickly
* [qwatch](https://gitlab.com/bradenbest/qwatch/tree/master) ([old](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/qwatch)) -- Quick Watch
* [dupefinder](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/dupefinder) -- Find duplicate files in a directory
* [dltools](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/dltools) ([old](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/dltools-old)) -- some functions to aid in downloading large amounts of videos
* [screencast](https://gitlab.com/bradenbest/shell-scripts/tree/master/scripts/screencast) -- a simple GIF screencaster
